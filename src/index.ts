import {CheckUpperCase} from "./UpperCaseChecker";
import {IsLengthValid} from "./IsLengthValid";
import {checkSpecial} from "./SpecialCaseChecker";
import {CheckLowerCase} from "./CheckLowerCase";
import {CheckNumerical} from "./CheckNumerical";

export function CheckPassword(password: string): boolean {
    if (!IsLengthValid(password)) {
        return false;
    }

    if (!CheckUpperCase(password)) {
        return false;
    }
    if (!CheckNumerical(password)){
        return false;
    }
    if(!CheckLowerCase(password)) {
        return false;
    }
    if(!checkSpecial(password)) {
        return false;
    }
    return true;
}