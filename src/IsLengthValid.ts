export function IsLengthValid(password: string): boolean {
    return password.length >= 12;
}