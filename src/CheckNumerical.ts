export function CheckNumerical(password: string): boolean {

    for (let i = 0; i < password.length; i++) {
        if ((password.charCodeAt(i) >= 48) && (password.charCodeAt(i) <= 57)) {
            return true;
        }
    }
    return false;
}