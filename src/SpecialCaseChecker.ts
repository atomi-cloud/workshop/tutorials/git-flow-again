export function checkSpecial(password: string): boolean {
    // Checks whether the string is only letters and numbers
    return !/^[a-zA-Z0-9]*$/.test(password);
}