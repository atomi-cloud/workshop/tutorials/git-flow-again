export function CheckLowerCase(password: string): boolean {
    let lowerCaseCharacters = password.split('').filter(function (x) {
        return (x.charCodeAt(0) > 96 && x.charCodeAt(0) < 123);
    });
    return lowerCaseCharacters.length > 0;
}