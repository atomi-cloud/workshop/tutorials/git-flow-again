export function CheckUpperCase(i: string): boolean {
    return i.split('').filter(x => x.toLowerCase() !== x).length > 0;
}