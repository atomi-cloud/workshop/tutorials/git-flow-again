import {should} from "chai";
import {CheckUpperCase} from "../src/UpperCaseChecker";

should();

describe("UpperCaseChecker", () => {

    it("should return true if there is a uppercase", () => {
        CheckUpperCase("Abcde123").should.be.true;
        CheckUpperCase("POKEMON").should.be.true;
    });

    it('should return false if there is no uppercase', function () {
        CheckUpperCase("abcde12345").should.be.false;
        CheckUpperCase("pokemon").should.be.false;
    });

});