import {should} from "chai";
import {IsLengthValid} from "../src/IsLengthValid";

should();

describe("CheckLength", () => {
    it("should return false if length is less that 12 characters", () => {
        const password1 = "poop";
        const password2 = "0123456789A";
        IsLengthValid(password1).should.be.false;
        IsLengthValid(password2).should.be.false;
    });

    it("should return true if length is equal to 12 characters", () => {
        const password = "0123456789AB";
        IsLengthValid(password).should.be.true;
    });

    it("should return true if length is greater that 12 characters", () => {
        const password1 = "0123456789ABC";
        const password2 = "FOIWFJIOWEJFIO#@#JFF#";
        IsLengthValid(password1).should.be.true;
        IsLengthValid(password2).should.be.true;
    });
});