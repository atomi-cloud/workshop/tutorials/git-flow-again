import {should} from "chai";
import {CheckPassword} from "../src";

should();

describe("Check Password",()=>{
    describe("Check Upper Failures", () =>{
        it("No Caps only, Num first",()=>{
            CheckPassword("12ab!@sdjife").should.be.false;
        });

        it("No Caps only, Lower first",()=>{
            CheckPassword("ab34!@sdjife").should.be.false;
        });

        it("No Caps only, Special first",()=>{
            CheckPassword("!@12absdjife").should.be.false;
        });
    });

    describe("Check Lower Failures", () =>{
        it("No Lower only, Num first",()=>{
            CheckPassword("12AB!@SDJIFE").should.be.false;
        });

        it("No Lower only, Upper first",()=>{
            CheckPassword("AB34!@SDJIFE").should.be.false;
        });

        it("No Lower only, Special first",()=>{
            CheckPassword("!@12ABSDJIFE").should.be.false;
        });
    });

    describe("Check Number Failures", () =>{
        it("No Num only, Caps first",()=>{
            CheckPassword("YEab!@sdjife").should.be.false;
        });

        it("No Num only, Lower first",()=>{
            CheckPassword("abYE!@sdjife").should.be.false;
        });

        it("No Num only, Special first",()=>{
            CheckPassword("!@YEabsdjife").should.be.false;
        });
    });

    describe("Check Special Failures", () =>{
        it("No Special only, Num first",()=>{
            CheckPassword("12abYEsdjife").should.be.false;
        });

        it("No Special only, Lower first",()=>{
            CheckPassword("ab34YEsdjife").should.be.false;
        });

        it("No Special only, Caps first",()=>{
            CheckPassword("YE12absdjife").should.be.false;
        });
    });

    describe("Check Length Failures", () =>{
        it("Short Length only, Num first",()=>{
            CheckPassword("12AB!@sdji").should.be.false;
        });

        it("Short Length only, Lower first",()=>{
            CheckPassword("ab34!@SDji").should.be.false;
        });

        it("Short Length only, Upper first",()=>{
            CheckPassword("AB34!@sdji").should.be.false;
        });

        it("Short Length only, Special first",()=>{
            CheckPassword("!@12ABsdji").should.be.false;
        });
    });

    describe("Check All Present", () =>{
        it("Num first",()=>{
            CheckPassword("12AB!@sdji11").should.be.true;
        });

        it("Lower first",()=>{
            CheckPassword("ab34!@SDji23").should.be.true;
        });

        it("Special first",()=>{
            CheckPassword("!@12ABsdji34").should.be.true;
        });

        it("Caps first",()=>{
            CheckPassword("AB!@12sdji34").should.be.true;
        });
    });

});