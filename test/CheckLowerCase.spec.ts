import {should} from "chai";
import {CheckLowerCase} from "../src/CheckLowerCase";

should();

describe("CheckLowerCase",()=>{

    it("should return true if there is at least 1 lowercase letter",()=>{
        CheckLowerCase('a').should.be.true;
        CheckLowerCase('bC').should.be.true;
        CheckLowerCase('defghi').should.be.true;
        CheckLowerCase('jklmnopqrstuvwxyz').should.be.true;
    });

    it("should return false if there are no lowercase letters", ()=>{
        CheckLowerCase('').should.be.false;
        CheckLowerCase('!@&$^UASHFIHKJVN').should.be.false;
    })

});