import {should} from "chai";
import {CheckNumerical} from "../src/CheckNumerical";

should();

describe("CheckNumericals", () => {

    it("if there's at least 1 numerical return true", () => {
        CheckNumerical("abcd123").should.equal(true);
        CheckNumerical("ab1d3").should.equal(true);
        CheckNumerical("ab2d123").should.equal(true);
        CheckNumerical("ab1d123").should.equal(true);
    });
    it("if there's no numerical return false", () => {
        CheckNumerical("abcd%!@").should.equal(false);
        CheckNumerical("abcdddd").should.equal(false);
        CheckNumerical("abcaaa").should.equal(false);

    });

});