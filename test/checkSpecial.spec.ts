import {should} from "chai";
import {checkSpecial} from "../src/SpecialCaseChecker";

should();

describe("CheckSpecial", () => {
    it("should return true if there is a special character", () => {
        checkSpecial("1234$").should.equal(true);
    });

    it("should return false if there isn't a special character", () => {
        checkSpecial("1234").should.equal(false);
    });

    it("should return false if no special character", () => {
        checkSpecial("asdf1^$234%341").should.equal(true);
    });
});